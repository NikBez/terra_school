from environs import Env

ENV = Env()
ENV.read_env()

DB_USER = ENV('db_user')
DB_PASSWORD = ENV('db_password')
DB_HOST = ENV('db_host')
DB_PORT = ENV('db_port')
DB_NAME = ENV('db_name')