from enum import Enum

from fastapi import FastAPI
from pydantic import BaseModel
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from config import DB_HOST, DB_NAME, DB_PASSWORD, DB_PORT, DB_USER
from models import User, create_tables


dns = f'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'
engine = create_engine(dns)
create_tables(engine)
Session = sessionmaker(bind=engine)
session = Session()

app = FastAPI()


class Genders(Enum):
    male = 'male'
    female = 'female'
    another = 'another'


class AddUser(BaseModel):
    name: str
    gender: Genders


@app.get('/users', description='Get all users')
def main():
    users = session.query(User).all()
    return users


@app.get('/users/{gender}')
def get_by_genders(gender: Genders, limit: int | None = None):
    users = session.query(User).filter(User.gender == gender.value).limit(limit).all()
    return users


@app.post('/users/add', response_model=list[AddUser])
def add_user(users: list[AddUser]):
    for user in users:
        new_user = User(name=user.name.capitalize(), gender=user.gender.value)
        session.add(new_user)
    session.commit()
    return users

session.close()
