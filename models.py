from sqlalchemy import CheckConstraint, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    gender = Column(String)
    __table_args__ = (
        CheckConstraint("gender in ('male', 'female', 'another')",
                        name='gender_constraint'
                        ),
    )

    def __str__(self):
        return f'{self.id}: {self.name}, {self.gender}'


def create_tables(engine):
    Base.metadata.create_all(engine)


